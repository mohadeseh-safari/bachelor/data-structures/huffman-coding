
public class Tokenizer {
    String expression;
    Character delimeter;
    int pointer=0;
    Tokenizer(String exp,Character deli){
    expression= exp;
    delimeter = deli;    
    }
    
    public String next(){
    String result = "";
    if(!hasNext())
        return "End of expression";
    while(expression.charAt(pointer)==delimeter)
        pointer ++;
    while( pointer<expression.length() && expression.charAt(pointer)!=delimeter ){
    result += expression.charAt(pointer);
    pointer ++;
    }
    pointer ++;
    return result; 
    }// End of next()
    
    public boolean hasNext(){
    return ((pointer <= expression.length()-1));    
    } // End of hasNext()
       
}
