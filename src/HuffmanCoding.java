import java.io.*;
import java.util.HashMap;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HuffmanCoding {
    //HashMap
    //MyMap
    static Scanner input = new Scanner(System.in);
    static HashMap<Character, Integer> map = new HashMap();
    static HashMap<Character,TNode> char_t = new HashMap();
    static File dest = new File("destination.txt");
    static FileWriter fr;
    static TNode root;
    static TNode it;
    static TNode[] t_leaves;
    static int size = 0;
    static class TNode {

        TNode parent = null;
        TNode right = null;
        TNode left = null;
        String txt = "";
        int value = 0;
        String code ; // گره های غیر برگ کد نال دارند و این مساله راه شناسایی خوبی برای برگ است
        
        public TNode() {

        }

        public TNode(String t, int v) {
            txt = t;
            value = v;
        }

        public TNode(TNode l, TNode r, String t, int v) {
            left = l;
            right = r;
            txt = t;
            value = v;
            r.parent = this;
            l.parent = this;
        }

        static class PQueue {

            int size = 0;
            int first = 0;
            TNode[] data;

            PQueue(int capacity) {
                data = new TNode[capacity];
                for (int i = 0; i < capacity; i++) {
                    data[i] = new TNode();
                }

            }

            PQueue() {
                data = new TNode[100];
                for (int i = 0; i < 100; i++) {
                    data[i] = new TNode();
                }
            }

            public void enqueue(TNode element) {
                data[(first + size) % data.length] = element;
                size++;
                sortLastElm();
            }

            public void sortLastElm() {
                int last = (first + size - 1) % data.length;
                int iterate = size - 1;
                while (data[last].value >= data[(last + data.length - 1) % data.length].value && iterate > 0) {
                    if(data[last].value == data[(last + data.length - 1) % data.length].value){
                    if(data[last].txt.charAt(0)< data[(last + data.length - 1) % data.length].txt.charAt(0))
                    break;
                    }
                    iterate--;
                    TNode temp = data[last];
                    data[last] = data[(last + data.length - 1) % data.length];
                    data[(last + data.length - 1) % data.length] = temp;
                    last = (last + data.length - 1) % data.length;
                }
            }

            public TNode dequeue() {
                TNode result = data[(first + size - 1) % data.length];
                size--;
                return result;
            }

        } // EndOfPQueue
    }

    public static void main(String[] args) throws IOException {

        System.out.println("Choose the task to be done :(type number)\n*note that the input file name shouldbe text.txt and it will be coded in bina.txt.and for decoding the output will be written in destination.txt\n 1.compress string \n 2.compress text from file\n 3.Extract a binary file  ");
         int choise = input.nextInt(); 
         if(choise==1){
         System.out.println("Enter the String");
         String text = input.next();
         writeInFile(text);
         }else if(choise==2)
          writeInFile();   
         else
         readFromFile();
 
    }
    
    public static void read_in_byte(byte b , int n){
    if(n==7)
    return ;
  
    read_in_byte((byte)(b/2),n+1);
    
    if(b%2==0)
    it = it.left ;
    else 
    it = it.right ;
    
    if(it.code != null){
        try {
        // به برگ رسید
        fr.write(it.txt);
        } catch (IOException ex) {
            Logger.getLogger(HuffmanCoding.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    it = root ;
    }       
    } 
    
    //MyMap
   /* static TNode[] generateLeaves() {
        TNode[] leaves = new TNode[map.size()];
        int i = 0;
        Character c ;
        MyMap.Node<Character,Integer> p = map.head;
        while(p.next != map.head){
            c = p.key;
            leaves[i] = new TNode(c + "", p.value);
            char_t.put(leaves[i].txt.charAt(0),leaves[i]);
            i++;
            p = p.next;
        }
        c = p.key;
        leaves[i] = new TNode(c + "", p.value);
        char_t.put(leaves[i].txt.charAt(0),leaves[i]);
        return leaves;
    }*/
   
            static TNode[] generateLeaves() {
            TNode[] leaves = new TNode[map.size()];
            int i = 0;
            for (Character c : map.keySet()) {
            leaves[i] = new TNode(c + "", map.get(c));
            char_t.put(leaves[i].txt.charAt(0),leaves[i]);
            i++;
            }
            return leaves;
            }
    
        static void readFromFile() throws IOException{ 
        BufferedReader fis = new BufferedReader(new FileReader("bina.txt"));
        
        //{read contract
         
        String s = fis.readLine();
        int s_length = Integer.parseInt(s);
        String conv ="";
        for(int i=0;i<s_length;i++){
        conv += (char)fis.read();
        }
        System.out.print(conv);    
        fis.readLine();
        //read contract}
        
        //Generating map
        Tokenizer tokenizer = new Tokenizer(conv,'^');
        map = new HashMap();
        String token="";
        while(tokenizer.hasNext()){
        token = tokenizer.next();
        map.put(token.charAt(0),Integer.parseInt(token.substring(2)));
        }
     
        // 
         
        // char_T map
        char_t = new HashMap();
   
        //Generate tree
       handler();

        BufferedReader br = new BufferedReader(new FileReader("byte_size.txt"));
        size = Integer.parseInt(br.readLine());
        
        byte m = 0;
       
        it = root;
        fr = new FileWriter(dest);
        for(int i=0;i<(size/7);i++){
                m = (byte)fis.read(); 
                read_in_byte(m,0);
        }
       fr.close();
 
        }    
        
        
        static void writeInFile() throws IOException{
        Character ch;
        String str ;
        BufferedReader reader = new BufferedReader(new FileReader("text.txt"));
        str = reader.readLine();
        
        while(str!=null){
         for(int i=0; i<str.length();i++){
         ch =str.charAt(i);
         if(map.containsKey(ch)){
         map.put(ch,map.get(ch)+1);
         }
         else 
         map.put(ch,1);         
         } 
         str = reader.readLine();
        }
      
       handler();
       
       
       reader = new BufferedReader(new FileReader("text.txt"));
       fr = new FileWriter(new File("bina.txt"));
       
       
       //{ write Contract
       String conv = "";
       for(int i=0;i<t_leaves.length;i++){
       conv += t_leaves[i].txt.charAt(0)+":"+t_leaves[i].value+"^";
       }
       //conv +="start";

       fr.write(conv.length()+"\n");
       for(int i=0;i<conv.length();i++){
       fr.write((byte)conv.charAt(i));
       }
       fr.write("\n");
       
       
       
       byte m = 0;// پیمانه
       int k=0;
       String t ;
       str = reader.readLine();
       while(str !=null){
       for(int i = 0;i<str.length();i++){
       t = char_t.get(str.charAt(i)).code;
       for(int j=0;j<t.length();j++){ // به اندازه طول کد کاراکتر
         m = (byte)(m*2);    
         if(t.charAt(j)== '1')
         m++; 
         k++;
         size ++ ;
         if(k==7){
         fr.write(m);
         m = 0;
         k = 0;
         }
       }
       }
       str = reader.readLine();    
       }
        fr.close();
        String si = size+"" ;
        FileWriter br = new FileWriter ("byte_size.txt");
        br.write(si);
        br.close();
        }       
        
        
        static void writeInFile(String s) throws IOException{
        Character ch;
        String str = input.nextLine() ;
        for(int i=0; i<str.length();i++){
         ch =str.charAt(i);
         if(map.containsKey(ch)){
         map.put(ch,map.get(ch)+1);
         }
         else 
         map.put(ch,1);         
         } 
        handler();
        fr = new FileWriter(new File("bina.txt"));
        
        //{ write Contract
       String conv = "";
       for(int i=0;i<t_leaves.length;i++){
       conv += t_leaves[i].txt.charAt(0)+":"+t_leaves[i].value+"^";
       }
       fr.write(conv.length()+"\n");
       
       for(int i=0;i<conv.length();i++){
       fr.write((byte)conv.charAt(i));
       }
       fr.write("\n");
       
       byte m = 0;// پیمانه
       int k=0;
       String t ;
     
      
       for(int i = 0;i<str.length();i++){
       t = char_t.get(str.charAt(i)).code;
       for(int j=0;j<t.length();j++){ // به اندازه طول کد کاراکتر
         m = (byte)(m*2);    
         if(t.charAt(j)== '1')
         m++; 
         k++;
         size ++ ;
         if(k==7){
         fr.write(m);
         m = 0;
         k = 0;
         }
       }
       }    
        fr.close();
        String si = size+"" ;
        FileWriter br = new FileWriter ("byte_size.txt");
        br.write(si);
        br.close();
       
        }   
        
        static void handler(){
        t_leaves = generateLeaves();
       generateTree(t_leaves);
       for(int i=0;i<t_leaves.length;i++){
       findCode(t_leaves[i]); 
       System.out.println(t_leaves[i].txt + " : " + t_leaves[i].code);
        }    
        }
            
    static void generateTree(TNode[] leaves) {
        TNode.PQueue q = new TNode.PQueue();
        for (TNode node : leaves) 
            q.enqueue(node);
       
        while (q.size >= 2) {
            TNode l = q.dequeue();
            TNode r = q.dequeue();
            TNode t = new TNode(l, r, "" + r.txt + l.txt , l.value + r.value);
            q.enqueue(t);
            if(q.size==1)
                root = q.dequeue();
        }
    }
    static void inorder(TNode t){
    if(t != null){
    inorder(t.left); 
    System.out.println(t.txt);
    inorder(t.right);
    }       
    }
    
    static void findCode(TNode t){
    String code="";
    TNode p = t ;    
    while(p != root){
    if(p.parent.left == p)
        code = "0" + code ;
    else 
        code = "1" + code ;
    p = p.parent;
    } 
    t.code = code ;
    }

}
