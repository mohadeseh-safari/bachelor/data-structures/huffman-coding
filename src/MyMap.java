
import java.util.Set;


 public class MyMap<K,V> {

    int size = 0;
    Node<K,V> head;
    Node<K,V> tail;
    Set<K> key_set;

    MyMap(){
        
    }
    
    void put(K key , V value){
    if(head==null){ // map is empty
    Node<K,V> node = new Node(key,value,null);
    head = node;
    tail = node;
    node.next = head;
    size = 1 ;
    }
    else{        
    Node<K,V> beforeNode = FindBeforeKeyNode(key);   
    if(beforeNode==null){ // key doesn't exist in map
    Node<K,V> node = new Node(key,value,null);
    tail.next=node;
    tail = node;
    tail.next = head;
    size ++;
    } else // key already exists in map and value must update
    beforeNode.next.value= value; 
    }  
    }
    
    static class Node<K,V> {
    public K key;
    public V value;
    public Node<K,V> next;
    V getValue() {
    return this.value;
    }
    
    K getKey() {
    return this.key;
    }
    
    Node(K key,V value,Node<K,V> next ){
    this.key = key;  
    this.value = value;  
    this.next = next;  
    }

    
    }   
    
    V get (K key){
    Node<K,V> before = FindBeforeKeyNode(key);
    if(before==null)
        return null;
    return before.next.value;
    }
    
    Boolean containsKey(K key){
    if(size==0)
    return false;
    Node<K,V> before = FindBeforeKeyNode(key);
    if(before==null)
    return false;
    return true;
    }
    
    Set<K> keySet(){
      if(size==1){
       key_set.add(head.key);  
       return key_set;
      }
      
     Node<K,V> p = head ;
     while(p.next != head){
     key_set.add(p.key);
     p = p.next;
     }
     key_set.add(p.key); // adds tail
     return key_set;
    }
    
    int size(){
    return size;    
    }
    
    Node<K,V> FindBeforeKeyNode(K key){ // finds the Node which it's next has the key in methods sign  
        if(key==null)
            return null;
        Node<K,V> p = head ;
    if(key.equals(head.key)){
     //  System.out.println(0);
       return tail;
    }
    
    if(size==1){// only head exixsts and similar key wasn't found
     // System.out.println(1);
      return null;

    }
    while(p.next != head){
    if(p.next.key==key){
       //  System.out.println(p.key); 
        return p ; 
    }
    p = p.next;
    }
    return null;
    }
        
}
